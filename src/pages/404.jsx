import React from 'react'

const NotFound = () => {
  return (
    <div><p>Nothing to see here.</p></div>
  )
}

export default NotFound
