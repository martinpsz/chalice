import React from 'react'
import { graphql } from "gatsby"
import '../base.css'
import Seo from '../components/SEO'
import {Header} from '../components/Header/Header'
import {About} from '../components/About/About'
import Products from '../components/Products/Products'
import Partners from '../components/Partners/Partners'
import Contact from '../components/Contact/Contact'
import {useI18next} from 'gatsby-plugin-react-i18next';
import LanguageContext from '../contexts/Language'

export default function Home() {
  const {language} = useI18next();

  return (
    <LanguageContext.Provider value={language}>
      <Seo/>
      <Header/>
      <main className="container">
        <About/>
        <Products/>
        <Partners/>
      </main>
      <Contact/>
    </LanguageContext.Provider>
  )
}


export const query = graphql`
  query($language: String!){
    locales: allLocale(filter: {language: {
      eq: $language
    }}) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }

`