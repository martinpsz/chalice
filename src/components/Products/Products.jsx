import React, {useState} from 'react'
import ProductSelection from './ProductSelection'
import { SectionHeader } from '../SectionHeader'
import styled from 'styled-components'
import WineProducts from './Queries/wineProducts'
import BeerProducts from './Queries/beerProducts'
import OtherProducts from './Queries/otherProducts'
import {useI18next, Trans} from 'gatsby-plugin-react-i18next';
import Product from './Product'

const StyledProducts = styled.section`
    
    .products-container{
        padding: 0 0.5em 1em;
        display: grid;
        grid-auto-flow: column;
        grid-auto-columns: 95%;
        overflow-x: scroll;
        overscroll-behavior-inline: contain;
        scroll-snap-type: inline mandatory;
        scroll-padding-inline: 0.5rem;
        grid-gap: 0.25em;
        
        @media (min-width: 450px){
            grid-auto-columns: calc(95% - 20vw);
        }

        @media (min-width: 768px){
            grid-auto-flow: unset;
            grid-template-columns: repeat(2, 1fr);
            grid-gap: 1em;
            padding: 0 1em;
        }

        @media (min-width: 992px){
            grid-template-columns: repeat(3, 1fr);
        }

        @media (min-width: 1320px){
            padding: 0;
        }
    }

    @media (min-width: 1320px){
        max-width: 75vw;
        margin: 0 auto;
    }

`
export default function Products() {
  const {language} = useI18next()
  const [selection, setSelection] = useState({})
  

const wines = WineProducts()
const beers = BeerProducts()
const others = OtherProducts()

const ProductPopulation = (currState) => {
    const mainCategory = currState.mainCategory
    const subCategory = (currState.mainCategory === 'Wines' | currState.mainCategory === 'Beers' )  ?   [...currState.subCategory]  : undefined

     if(mainCategory === 'Wines'){
        return wines.filter(wine => subCategory.includes(wine.node.wine_meta.varietal.split(":")[0].trim())).map(wine => {
            return (
               <Product       key ={wine.node.id}
                              img={wine.node.post_image.image.localFile.childImageSharp}
                              alt={wine.node.bilingual_post_titles.titleEn}
                              title={wine.node.bilingual_post_titles.titleEn}
                              producer={wine.node.product_producer_selected.producer.partner_meta_en.partnerNameEn}
                              region={wine.node.product_producer_selected.producer.partner_meta_en.partnerRegionEn}
                              country={wine.node.product_producer_selected.producer.partner_meta_en.partnerCountryEn}
                              retailOne={wine.node.pricing_details.retailPrice}
                              vipOne={wine.node.pricing_details.vipPrice}
                              itemNum={wine.node.pricing_details.howManyInACase}
                              retailMulti={wine.node.pricing_details.retailPricePerCase}
                              vipMulti={wine.node.pricing_details.vipPricePerCase} />
            )
        })
     } else if(mainCategory === 'Beers'){
            return beers.filter(beer => subCategory.includes(beer.node.beer_meta.style.split(':')[0].trim())).map(beer => {
                return(
                        <Product        key ={beer.node.id}
                                        img={beer.node.post_image.image.localFile.childImageSharp}
                                        alt={beer.node.bilingual_post_titles.titleEn}
                                        title={beer.node.bilingual_post_titles.titleEn}
                                        producer={beer.node.product_producer_selected.producer.partner_meta_en.partnerNameEn}
                                        region={beer.node.product_producer_selected.producer.partner_meta_en.partnerRegionEn}
                                        country={beer.node.product_producer_selected.producer.partner_meta_en.partnerCountryEn}
                                        retailOne={beer.node.pricing_details.retailPrice}
                                        vipOne={beer.node.pricing_details.vipPrice}
                                        itemNum={beer.node.pricing_details.howManyInACase}
                                        retailMulti={beer.node.pricing_details.retailPricePerCase}
                                        vipMulti={beer.node.pricing_details.vipPricePerCase} />
                )
            })
     } else if (mainCategory === 'Ciders'){
            return others.filter(other => other.node.productTypes.nodes[0].name === 'Ciders').map(other => {
                return (
                    <Product        key ={other.node.id}
                                    img={other.node.post_image.image.localFile.childImageSharp}
                                    alt={other.node.bilingual_post_titles.titleEn}
                                    title={other.node.bilingual_post_titles.titleEn}
                                    producer={other.node.product_producer_selected.producer.partner_meta_en.partnerNameEn}
                                    region={other.node.product_producer_selected.producer.partner_meta_en.partnerRegionEn}
                                    country={other.node.product_producer_selected.producer.partner_meta_en.partnerCountryEn}
                                    retailOne={other.node.pricing_details.retailPrice}
                                    vipOne={other.node.pricing_details.vipPrice}
                                    itemNum={other.node.pricing_details.howManyInACase}
                                    retailMulti={other.node.pricing_details.retailPricePerCase}
                                    vipMulti={other.node.pricing_details.vipPricePerCase} />
                )
            })
     } else if (mainCategory === 'Liqueurs'){
        return others.filter(other => other.node.productTypes.nodes[0].name === 'Liqueurs').map(other => {
            return (
                <Product        key ={other.node.id}
                                img={other.node.post_image.image.localFile.childImageSharp}
                                alt={other.node.bilingual_post_titles.titleEn}
                                title={other.node.bilingual_post_titles.titleEn}
                                producer={other.node.product_producer_selected.producer.partner_meta_en.partnerNameEn}
                                region={other.node.product_producer_selected.producer.partner_meta_en.partnerRegionEn}
                                retailOne={other.node.pricing_details.retailPrice}
                                vipOne={other.node.pricing_details.vipPrice}
                                itemNum={other.node.pricing_details.howManyInACase}
                                retailMulti={other.node.pricing_details.retailPricePerCase}
                                vipMulti={other.node.pricing_details.vipPricePerCase} />
            )
        })
 }
    
}

  return (
    <StyledProducts id="Selection">
        <SectionHeader><Trans>Our Selection</Trans></SectionHeader>
        <ProductSelection getSelection={setSelection} option={language}/>
        <div className='products-container'>
            {ProductPopulation(selection)}
        </div>
    </StyledProducts>
  )
}
