import React, {useContext} from 'react'
import LanguageContext from '../../contexts/Language'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'
import {GiWineBottle} from 'react-icons/gi'
import styled from 'styled-components'
import {GeneralButton} from '../Button'
import {FiShoppingCart} from 'react-icons/fi'
import {Trans} from 'gatsby-plugin-react-i18next'
    

const StyledProduct = styled.div`
    scroll-snap-align: start;
    display: grid;
    grid-template-columns: 30% 70%;
    grid-template-areas: 'title title'
                         'img data';
    align-items: center;
    justify-content: center;
    padding: 0.25em 0;
    border: 0.25px solid hsla(213, 74%, 15%, 0.25);
    border-radius: 0.25em;


    h3{
        grid-area: title;
        font-family: var(--copy-font-en);
        font-size: 1rem;
        color: var(--blue);
        text-transform: uppercase;
        padding: 0.25em;
        align-self: start;
        text-align: center;
        margin-bottom: 0.5em;
        position: relative;
        
        &:after{
            position: absolute;
            content: '';
            height: 1px;
            width: 25%;
            background: var(--red);
            bottom: -2px;
            left: 50%;
            transform: translateX(-50%);
        }
    }


    .product-img{
        align-self: start;
        grid-area: img;
        filter: drop-shadow(2px 2px 8px var(--blue));
    }

   
   .product-data{
        grid-area: data;
        margin-left: 1em;
        height: 90%;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;


        .product-data-meta{
            display: flex;
            flex-direction: column;
            font-family: ${props => props.language === 'zh' ? 'var(--copy-font-ch)' : 'var(--copy-font-en)'};
            color: var(--blue);
            font-weight: 400;
            

            span{
                text-transform: uppercase;
                font-size: 0.8em;
                font-weight: 400;
                color: var(--red);
            }
        }

        .product-data-pricing{
            font-family: ${props => props.language === 'zh' ? 'var(--copy-font-ch)' : 'var(--copy-font-en)'};;

            span{
                text-transform: uppercase;
                font-size: 0.8em;
                font-weight: 400;
                color: var(--red);
                
            }

            .product-pricing{
                display: flex;
                align-items: flex-end;

                &:nth-of-type(1){
                    margin-top: 0.1em;
                }
            }
        }

        button{
            color: white;
            font-size: 0.9em;
            align-self: flex-start;
            display: flex;
            align-items: center;

            @media (min-width: 768px){
                font-size: 1em;
            }
        }
   }        

`

export default function Product(props) {
    const language = useContext(LanguageContext)
    return (
        <StyledProduct className = "product-container" option={language}>
            <h3>{props.title}</h3>
            <div className="product-img">
                <GatsbyImage image={getImage(props.img)} alt={props.alt}/>
            </div>
            <div className="product-data">
                <p className="product-data-meta"><span><Trans>Producer</Trans>:</span>{props.producer}</p>
                <p className="product-data-meta"><span><Trans>Region</Trans>:</span>{props.region}, {props.country}</p>
                <div className="product-data-pricing">
                    <span><Trans>Pricing</Trans>:</span>
                    <p className="product-pricing">
                        <GiWineBottle size={32} color={"var(--red)"}/>
                        &times;1 - {props.retailOne} NT$
                    </p>
                    <p className="product-pricing">
                        <GiWineBottle size={32} color={"var(--red)"}/>
                        &times;{props.itemNum} - {`${props.retailMulti}` * `${props.itemNum}`} NT$
                    </p>
                </div>
                <GeneralButton background="var(--blue)">
                    <FiShoppingCart size={20} style={{marginRight: '0.5em'}}/><Trans>Add to Cart</Trans>
                </GeneralButton>
            </div>

        </StyledProduct>
    )
}
