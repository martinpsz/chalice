import { useStaticQuery, graphql } from "gatsby";

function OtherProducts() {
    const data = useStaticQuery(graphql`
    {
      allWpProduct(
        filter: {inventory: {inStock: {eq: "In Stock"}}, productTypes: {nodes: {elemMatch: {name: {in: ["Ciders", "Liqueurs"]}}}}}
      ) {
        edges {
          node {
            id
            productTypes {
              nodes {
                name
              }
            }
            bilingual_post_titles {
              titleEn
              titleCh
            }
            product_producer_selected {
              producer {
                ... on WpPartner {
                  partner_meta_en {
                    partnerRegionEn
                    partnerCountryEn
                    partnerNameEn
                  }
                  partner_meta_ch {
                    partnerRegionCh
                    partnerCountryCh
                    partnerNameCh
                  }
                }
              }
            }
            pricing_details {
              howManyInACase
              retailPrice
              retailPricePerCase
              vipPrice
              vipPricePerCase
            }
            post_image {
              image {
                localFile {
                  childImageSharp {
                    gatsbyImageData(
                      aspectRatio: 0.3
                      height: 600
                      formats: AUTO
                      backgroundColor: "transparent"
                      transformOptions: {fit: CONTAIN}
                      placeholder: NONE
                      quality: 100
                    )
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  const other = data.allWpProduct.edges

  return other
}

export default OtherProducts