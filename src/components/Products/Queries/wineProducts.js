import { useStaticQuery, graphql } from "gatsby";

function WineProducts() {
    const data = useStaticQuery(graphql`
    {
      allWpProduct(
        filter: {inventory: {inStock: {eq: "In Stock"}}, productTypes: {nodes: {elemMatch: {name: {eq: "Wines"}}}}}
      ) {
        edges {
          node {
            id
            bilingual_post_titles {
              titleEn
              titleCh
            }
            product_producer_selected {
              producer {
                ... on WpPartner {
                  partner_meta_en {
                    partnerRegionEn
                    partnerCountryEn
                    partnerNameEn
                  }
                  partner_meta_ch {
                    partnerRegionCh
                    partnerCountryCh
                    partnerNameCh
                  }
                }
              }
            }
            pricing_details {
              howManyInACase
              retailPrice
              retailPricePerCase
              vipPrice
              vipPricePerCase
            }
            post_image {
              image {
                localFile {
                  childImageSharp {
                    gatsbyImageData(
                        formats: AUTO
                        height: 600
                        aspectRatio: 0.3
                        backgroundColor: "transparent"
                        transformOptions: {fit: CONTAIN} 
                        placeholder: NONE
                        quality: 100        
                    )
                  }
                }
              }
            }
            wine_meta {
              varietal
            }
          }
        }
      }
    }
  `)


   const wines = data.allWpProduct.edges

   return wines
   
}

export default WineProducts