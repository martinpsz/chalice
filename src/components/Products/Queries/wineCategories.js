import {graphql, useStaticQuery} from 'gatsby'

const WineCategories = () =>  {
    const data = useStaticQuery(graphql`
    {
      allWpProduct(
        filter: {inventory: {inStock: {eq: "In Stock"}}, productTypes: {nodes: {elemMatch: {name: {eq: "Wines"}}}}}
      ) {
        edges {
          node {
            wine_meta {
              varietal
            }
          }
        }
      }
    }
  `)

  const varietals = []
  data.allWpProduct.edges.forEach(item => {
        if (varietals.indexOf(item.node.wine_meta.varietal) === -1){
            varietals.push(item.node.wine_meta.varietal)
        }
  })
 
  return varietals
}

export default WineCategories
