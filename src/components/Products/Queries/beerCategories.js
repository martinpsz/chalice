import {graphql, useStaticQuery} from 'gatsby'

const BeerCategories = () =>  {
    const data = useStaticQuery(graphql`
    {
      allWpProduct(
        filter: {inventory: {inStock: {eq: "In Stock"}}, productTypes: {nodes: {elemMatch: {name: {eq: "Beers"}}}}}
      ) {
        edges {
          node {
            beer_meta {
              style
            }
          }
        }
      }
    }
  `)

  const styles = []
  data.allWpProduct.edges.forEach(item => {
    if(styles.indexOf(item.node.beer_meta.style) === -1){
        styles.push(item.node.beer_meta.style)
    }
  })

  return styles
 
 
}

export default BeerCategories