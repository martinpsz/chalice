import React, {useContext} from 'react'
import LanguageContext from '../../contexts/Language'
import { useStaticQuery, graphql } from 'gatsby'
import styled from 'styled-components'
import WineCategories  from './Queries/wineCategories'
import BeerCategories from './Queries/beerCategories'
import { useReducer, useEffect} from 'react'
import {Trans} from 'gatsby-plugin-react-i18next';


const StyledSelections = styled.div`
    margin: 2em 0;

    .main-selections{
        display: flex;
        align-items: center;
        justify-content: center;
        gap: 0.15em;
        margin-bottom: 1em;
        
        
        button{
              background: white;
              border: 1px solid var(--blue);
              border-radius: 0.25em;
              padding: 0.75em 0.5em;
              color: var(--blue);
              text-transform: uppercase;
              font-family: ${props => props.option === 'zh' ? "var(--copy-font-ch)" : "var(--copy-font-en)"};
              display: flex;
              align-items: center;

              

              &.active{
                background: var(--red);
                border-color: var(--red);
                color: white;
              }

              span{
                margin-left: 0.2em;   
              }

              & > * {
                 pointer-events: none;
              }
        }

        @media (min-width: 360px){
            gap: 0.25em;

            button{
               padding: 0.75em 0.25em;

               span{
                  font-size: 0.9rem;
               }
            }
        }

        @media (min-width: 576px){
            button{
              padding: 0.75em;
            }
        }

    }

    .sub-selections{
        width: 90vw;
        padding-left: 0.25em;
        max-width: 1920px;
        margin: 0 auto;

        @media (min-width: 576px){
          padding-left: 2em;
        }

        @media (min-width: 1320px){
            padding-left: 0;
        }

        p{
          margin-bottom: 0.25em;
          color: var(--blue);
          font-family: ${props => props.option === 'zh' ? "var(--copy-font-ch)" : "var(--copy-font-en)"};
          font-size: 0.8rem;
          text-transform: uppercase;
          opacity: 0.8;
        }

        div{
          list-style: none;
          display: grid;
          grid-auto-flow: column;
          grid-auto-columns: 39.5%;
          grid-column-gap: 0.25em;
          padding-bottom: 1em;
          overflow-x: scroll;
          overscroll-behavior-inline: contain;
          scroll-snap-type: inline mandatory;
          

            &:empty{
              display: none;
            }

            @media (min-width: 576px){
               grid-auto-columns: 32.5%;
            }

            @media (min-width: 768px){
              grid-auto-columns: 17%;
            }

            @media (min-width: 1200px){
                grid-auto-columns: 11%;
            }


      
            button {
              background: white;
              font-family: ${props => props.option === 'zh' ? "var(--copy-font-ch)" : "var(--copy-font-en)"};
              font-size: 0.85rem;
              color: var(--blue);
              border: 1px solid var(--blue);
              border-radius: 0.25em;
              scroll-snap-align: start;
              text-transform: uppercase;
              text-align: center;
              padding: 0.25em;
              

                &.active{
                  background: var(--red);
                  border-color: var(--red);
                  color: white;
                }
            }  
        }

        @media (min-width: 1320px) {
              width: 75vw;

        }
    }
`


export default function ProductSelections(props) {
   const language = useContext(LanguageContext)
   
    const mainCategories =  useStaticQuery(graphql`
    {
      allWpProductType {
        edges {
          node {
            id
            name
          }
        }
      }
    }
  `)
   

const MainCats = () => mainCategories.allWpProductType.edges.map(cat=> {
  return <button key={cat.node.id}
                          value={cat.node.name}
                          className={state.mainCategory  === cat.node.name ? 'active' : null}
                          onClick={e => dispatch({type: 'update main category', payload: e.target.value})}>
                          <span><Trans>{cat.node.name}</Trans></span></button>
})

const varietals = []
WineCategories().forEach(val => {
     varietals.push({'Varietal_En' : val.split(':')[0].trim(),  'Varietal_Ch': val.split(':')[1].trim()})
  })
     
const styles = []
BeerCategories().forEach(val => {
   styles.push({'Style_En' : val.split(':')[0].trim(),  'Style_Ch': val.split(':')[1].trim()})
}) 

const WineCats = () => varietals.map(varietal => {
  
  return(
      <button key={varietal.Varietal_En}  data-key={varietal.Varietal_En}
                   className={state.subCategory.includes(varietal.Varietal_En) ? 'active' : null}
                   onClick={e => dispatch({type: 'update sub-category', payload: e.target.dataset['key']})}
                   onKeyDown = {e => dispatch({type: 'update sub-category', payload: e.target.dataset['key']})}>
                   {language === 'zh' ? `${varietal.Varietal_Ch}` : `${varietal.Varietal_En}`}
      </button>
  )
})

const BeerCats = () => styles.map(style => {
  return(
     <button key={style.Style_En}  data-key={style.Style_En} 
          className={state.subCategory.includes(style.Style_En) ? 'active' : null}
          onClick={e => dispatch({type: 'update sub-category', payload: e.target.dataset['key']})}
          onKeyDown={e => dispatch({type: 'update sub-category', payload: e.target.dataset['key']})}>
          {language === 'zh' ? `${style.Style_Ch}` : `${style.Style_En}`}
    </button>
  )
})

const initialState = {mainCategory: 'Wines', subCategory: ['Pinot Noir']}
const [state, dispatch] = useReducer(reducer, initialState)

function reducer(state, action){
    switch(action.type){
      case 'update main category':
        if(action.payload === 'Beers'){
          return {mainCategory: action.payload, subCategory: [`${styles[0].Style_En}`]}
        } else if (action.payload === 'Wines'){
          return {mainCategory: action.payload, subCategory: [`${varietals[0].Varietal_En}`]}
        } else {
          return {mainCategory: action.payload, subCategory: []}
        }
      case 'update sub-category':
        if(state.mainCategory === 'Wines'  |  state.mainCategory === 'Beers'){
          function results()  {
              if (state.subCategory.indexOf(action.payload) === -1){
                return  [...state.subCategory,  action.payload].slice(-3)
              } else {
                return [...state.subCategory].filter(val => val !== action.payload).slice(-3)
              }
          }
          return {...state, subCategory: results()};
        } 
      break;
      default:
        return state
    }
}

const CategoryHeader = () => {
   if(state.mainCategory === 'Wines'){
      return <p><Trans>Select up to three wine varietals</Trans>:</p>
   } else if (state.mainCategory === 'Beers'){
      return <p><Trans>Select up to three beer styles</Trans>:</p>
   }   
}

 
 const ProductsSelected = (data) => {
      useEffect(() => {
          props.getSelection(data)
      }, [data])
 }


  return (
    <StyledSelections onChange={ProductsSelected(state)} option={language}>
        <div className='main-selections'>
            <MainCats/> 
        </div>
        <div className='sub-selections'>
          {CategoryHeader()}
          <div>
                {state.mainCategory === 'Wines' && <WineCats/>}
                {state.mainCategory === 'Beers' && <BeerCats/>}
          </div>
        </div>
    </StyledSelections>
  )
}
