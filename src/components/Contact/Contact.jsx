import React, {useContext} from 'react'
import LanguageContext from '../../contexts/Language'
import styled from 'styled-components'
import wineglasses from '../../../static/images/wineglasses.jpeg'
import line from '../../../static/assets/line.svg'
import warning from '../../../static/images/warning.webp'
import {Trans} from 'gatsby-plugin-react-i18next'
import {useStaticQuery, graphql} from 'gatsby'
import {AiOutlineMail, AiFillFacebook} from 'react-icons/ai'
import {FiPhoneCall} from 'react-icons/fi'

const StyledContact = styled.footer`
    height: 480px;
    background-image: url(${wineglasses});
    background-color:hsla(213, 74%, 15%, 0.9);
    background-position: center center;
    background-size: cover;
    background-repeat: no-repeat;
    background-blend-mode: overlay;
    max-width: 1920px;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;


    #Contact-Info{
        color: white;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: space-evenly;
        height: 100%;
      
        h3{
            color: var(--yellow);
            font-family: ${props=> props.language === 'zh' ? "var(--header-font-ch)" : "var(--header-font-en)"};
            font-size: 3rem;
            font-weight: 400;
        }

        .contact-option{
            display: inline-flex;
            align-items: center;
            justify-content: center;
            text-transform: uppercase;
            padding: 0 1em;
            font-family: ${props => props.option === 'zh' ? "var(--copy-font-ch)" : "var(--copy-font-en)"};
            font-weight: 400;
            width: 300px;

            @media screen and (min-width: 768px){
                width: unset;
                font-size: ${props => props.option === 'zh' ? "1.5em" : "1em"};
            }
            
            }
            
            a{
                margin-left: 1em;
            }

            img{
                width: 2em;
                height: 2em;
            }

            #phone-icon{
                transform: rotate(250deg);
            }

        }


    #warning{
        color: white;
        padding: 0.25em 0;
        background-color:hsla(213, 74%, 15%, 0.9);
        width: 100vw;
        max-width: 1920px;
        text-align: center;
        

        p{
            font-family: ${props => props.option === 'zh' ? "var(--copy-font-ch)" : "var(--copy-font-en)"};
            font-weight: ${props => props.option === 'zh' ? 100 : 400};
            display: inline-flex;
            align-items: center;
            justify-content: center;
            text-transform: uppercase;
            color: var(--yellow);

            @media screen and (min-width: 768px){
                font-size: ${props => props.option === 'zh' ? '1.75rem': '1rem'};
            }


            img{
                margin-right: 0.5em;
                width: 2.5em;
                height: 2.5em;
            }
        }


        
    }

`

export default function Contact() {
  const data = useStaticQuery(graphql`
    {
      allWpMeta {
        edges {
          node {
            metaFields {
              email
              companyPhone
              companyFacebook
              companyLineAppLink
            }
          }
        }
      }
    }
  `)

  
  const {email, companyLineAppLink, companyFacebook, companyPhone} = data.allWpMeta.edges[0].node.metaFields
 

  const language = useContext(LanguageContext);
  
  return (
    <StyledContact id="Contact" option={language}>
        <div id='Contact-Info'>
            <h3><Trans>Contact Us</Trans></h3>
            <p className="contact-option"><Trans>Patrons in Taiwan</Trans>:<a href={companyLineAppLink} target="_blank" rel="noopener noreferrer" aria-label="Contact Chalice on the Line app"><img src={line} alt="contact Chalice via the LineApp" style={{width:'32px'}}/></a></p>
            <p className="contact-option"><Trans>International Patrons / Partners</Trans>:<a href={`mailto:${email}`} target="_blank" rel="noopener noreferrer" aria-label="Email Chalice"><AiOutlineMail size={32} color={"var(--yellow)"}/></a></p>
            <p className="contact-option">Ph: {companyPhone}<a href={`tel:${companyPhone}`} target="_blank" rel="noopener noreferrer" aria-label="Call Chalice"><FiPhoneCall size={32} color={"var(--yellow)"} id="phone-icon"/></a></p>
            <p className="contact-option"><Trans>Follow Chalice on</Trans>:<a href={companyFacebook} target="_blank" rel="noopener noreferrer" aria-label="Follow us on Facebook"><AiFillFacebook size={32} color={"var(--yellow)"}/></a></p>
        </div>
        <div id="warning">
            <p><img src={warning} alt="" className="icon" /><Trans>Don't Drink and Drive</Trans></p>
        </div>
    </StyledContact>
  )
}
