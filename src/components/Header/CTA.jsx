import React, {useContext} from 'react'
import LanguageContext from '../../contexts/Language';
import styled from 'styled-components'
import video from '../../../static/videos/video_HD.mp4'
import {Trans} from 'gatsby-plugin-react-i18next';
import {LanguageToggler} from '../LanguageToggler'
import {Link} from 'react-scroll'


const StyledCTA = styled.div` 
    position: relative;

    .btn{
        background: var(--red);
        color: var(--yellow);
        border: none;
        border-radius: 4px;
        padding: 0.75rem 1rem;
        text-transform: uppercase;
        font-family: ${props => props.option === 'zh' ? "var(--copy-font-ch)" : "var(--copy-font-en)"};
        font-size: 0.9rem;
        font-weight: 400;
        cursor: pointer;
        display: flex;
        justify-content: center;
        align-items: center;
        

        @media screen and (min-width: 768px){
            font-size: 1rem;
            padding: 1rem 1.5rem;
        }
    }

    .video-container{
        background: var(--blue); 
    }

    video{
        object-fit: cover;
        object-position: center center;
        height: 100vh;
        width: 100%;
        mix-blend-mode: luminosity;
        opacity: 10%;
        
    }

    h1{
        font-size: clamp(2rem, 5vmax, 4rem);
        line-height: 1.3;

        @media screen and (orientation: landscape){
            font-size: clamp(2rem, 3.5vmax, 4rem);
        }
    }

    @media screen and (min-width: 992px){
        video{
            opacity: 40%;
            max-height: 1080px;
        }
    }

    .overlay{
        position: absolute;
        top: 40%;
        width: 100%;
        text-align: center;
        max-width: 600px;
        border-radius: 0.5rem;
        padding: 0.5rem;

        h1{
            color: var(--yellow);
            font-family: ${props => props.option === 'zh' ? "var(--header-font-ch)" : "var(--header-font-en)"};
            font-weight: ${props => props.option === 'zh' ? 300 : 500};    
        }

        .ctaButtons{
            display: inline-flex;
            align-content: center;
            margin-top: 1.5rem;
            
            
            button:nth-of-type(1){
                margin-left: 1rem;
                
            }
        }

        @media screen and (orientation:landscape){
            top: 15%;
            width: 90%;
            left: 5%;
            right: 5%;
        }

        @media screen and (min-width: 768px){
            text-align: start;
            margin-left: 2.5rem;
            left: 0;
            top: 55%;
        }
  
    }  
    
`

export const CallToAction = () => {
    const language = useContext(LanguageContext)
    return (
        <StyledCTA option={language}>
            <div className='video-container'>
                <video autoPlay loop muted playsInline>
                    <source src={video} type="video/mp4"/>
                </video>
            </div>
            <div className='overlay'>
                <h1><Trans>Imported experiences for the Taiwanese palate</Trans></h1>
                <div className='ctaButtons'>
                    <Link to="About" 
                          smooth={true}
                          duration={1000} 
                          offset={-80}
                          className="btn">
                          <Trans>Learn More</Trans>
                    </Link>
                    <LanguageToggler/>
                </div>
            </div>
        </StyledCTA>
    )
}




