import React from 'react'
import {Nav} from '../Navigation/Nav'
import {CallToAction} from './CTA'
import styled from 'styled-components'

const StyledHeader = styled.header`
    max-width: 1920px;
    margin: 0 auto;
`


export const Header = () => {
    return(
        <StyledHeader>
            <Nav/>
            <CallToAction/>
        </StyledHeader>
    )
    
}