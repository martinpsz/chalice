import React, {useContext} from 'react'
import styled from 'styled-components'
import LanguageContext from '../contexts/Language'

const StyledButton = styled.button`
  background: ${props => props.background};
  color: var(--yellow);
  border: none;
  border-radius: 4px;
  padding: 0.75rem 1rem;
  text-transform: uppercase;
  font-family: ${props => props.option === 'zh' ? "var(--copy-font-ch)" : "var(--copy-font-en)"};
  font-size: 0.9rem;
  font-weight: 400;
  

  @media screen and (min-width: 768px){
      font-size: 1rem;
      padding: 1rem 1.5rem;
  }
`

export const GeneralButton = (props) => {
  const language = useContext(LanguageContext)
  return (
    <StyledButton background={props.background}
                  option={language} 
                  onClick={props.clickHandler}>{props.children}</StyledButton>
  )
}

