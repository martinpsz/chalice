import React, {useContext} from 'react'
import LanguageContext from '../../contexts/Language';
import styled from 'styled-components'
import { useStaticQuery, graphql } from 'gatsby'
import { SectionHeader } from '../SectionHeader';
import {GatsbyImage, getImage} from 'gatsby-plugin-image';


const StyledAbout = styled.section`
    font-family: ${props => props.option === 'zh' ? "var(--header-font-ch)" : "var(--header-font-en)"};

    .text{
        font-family: ${props => props.option === 'zh' ? "var(--copy-font-ch)" : "var(--copy-font-en)"};
        font-weight: 400;
        padding: 0 1rem;

        p{
            line-height: 1.5;
            color: var(--blue);
            margin: 1rem 0;
        }
    }

    .gatsby-image-wrapper{
        margin: 0 auto;
        
        
    }

    

    @media screen and (min-width: 768px){
        
        section{
            div:nth-of-type(2){
                max-width: 95vw;
                margin: 0 auto;


                .gatsby-image-wrapper{
                    float: right;
                }    
            }  
        } 

        section:nth-of-type(1){
            div:nth-of-type(2){
                .gatsby-image-wrapper{
                    max-width: 45vmin;
                    margin-left: 1rem;
                }   
            }
        }

        section:nth-of-type(2){
            div:nth-of-type(2){
                .gatsby-image-wrapper{
                    float: left;
                    margin: 0 1em 0;
                }
                
            }
        }   
    }


    @media screen and (min-width: 1320px){
        section{
            max-width: 75vw;
            margin: 0 auto;

            .gatsby-image-wrapper{
                aspect-ratio: 1.2;
            }
        }
    }

    @media screen and (max-height: 450px) and (orientation: landscape){
        .gatsby-image-wrapper{
            aspect-ratio: 0.9;
            max-width: 85vmin;
            margin-left: 1rem;
        }

    }
   
`

export const About = () => {
  const language = useContext(LanguageContext)
  const data = useStaticQuery(graphql`
    query MyQuery {
        allWpAbout {
        nodes {
            bilingual_post_titles {
            titleCh
            titleEn
            }
            bilingual_content {
            contentCh
            contentEn
            }
            post_image {
            image {
                localFile {
                childImageSharp {
                    gatsbyImageData(
                        placeholder: TRACED_SVG
                        formats: WEBP
                        webpOptions: {quality: 100}
                        transformOptions: {fit: COVER}
                        layout: FIXED
                        width: 320
                      )
                }
                }
                altText
            }
            }
        }
        }
    }
  
  `)

  

  const Section = (props) => {
      return(
        <section>
            <SectionHeader>{props.title}</SectionHeader>
            <div>
                <div>
                    <GatsbyImage image={props.image} alt={props.alt}/>
                </div>
                <div className="text" dangerouslySetInnerHTML={{__html: props.text}}/>
            </div>
        </section>

      )
  }

  const LanguageValues = () => {
      return(
      language === 'zh' ? 
            data.allWpAbout.nodes.map(node => {
                return (
                    <Section key = {node.bilingual_post_titles.titleCh}  
                            title={node.bilingual_post_titles.titleCh} 
                            image={getImage(node.post_image.image.localFile.childImageSharp)}
                            alt={node.post_image.image.altText}
                            text={node.bilingual_content.contentCh}/>
                )
            }).reverse() :
            data.allWpAbout.nodes.map(node => {
                return(
                    <Section key = {node.bilingual_post_titles.titleEn}  
                            title={node.bilingual_post_titles.titleEn} 
                            image={getImage(node.post_image.image.localFile.childImageSharp)}
                            alt={node.post_image.image.altText}
                            text={node.bilingual_content.contentEn}/>
                )
            }).reverse()
      )
    }
    
  
  
  return <StyledAbout id="About" option={language}>{LanguageValues()}</StyledAbout>
  
  
  
}

