import React from 'react';
import styled from 'styled-components';
import {useI18next} from 'gatsby-plugin-react-i18next';


const Button = styled.button`
  background: var(--lgt-blue);
  color: var(--yellow);
  border: none;
  border-radius: 4px;
  padding: 0.75rem 1rem;
  text-transform: uppercase;
  font-family: ${props => props.option === 'zh' ? "var(--copy-font-en)" : "var(--copy-font-ch)"};
  font-size: 0.9rem;
  font-weight: 400;
  cursor: pointer;

  @media screen and (min-width: 768px){
      font-size: 1rem;
      padding: 1rem 1.5rem;
  }
`

export const LanguageToggler = () => {
  const {language, changeLanguage} = useI18next();

  const renderLink = () => {
    if (language==='zh'){
        return <Button option={language} onClick={() => changeLanguage('en')}>English</Button>
    } else {
        return <Button option={language} onClick={() => changeLanguage('zh')}>中文</Button>
    }
}


  return renderLink()
   
};

