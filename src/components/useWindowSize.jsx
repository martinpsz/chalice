import {useState, useEffect} from 'react'




export const useWindowSize = () => {

    const isBrowser = typeof window !== 'undefined'

    let windowWidth;
    
    if (isBrowser) {
        windowWidth = window.innerWidth
    }

    const [width, setWidth] = useState(windowWidth)

    const windowWidthUpdater = () => {
        setWidth(window.innerWidth)
    }

    useEffect(()=>{
        window.addEventListener("resize", windowWidthUpdater);
        return () => {
            window.removeEventListener("resize", windowWidthUpdater)
        }
    }, [])
    
    return {
        width
    }
}

