import React, {useContext} from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import {Helmet} from 'react-helmet'
import Icon from '../../static/images/ChaliceIco.webp'
import LanguageContext from '../contexts/Language'


export default function SEO() {
  const data = useStaticQuery(graphql`
    {
      allWpMeta {
        edges {
          node {
            metaFields {
              titleEn
              titleCh
              siteDescriptionEn
              siteDescriptionCh
            }
          }
        }
      }
    }
  `)
  
  const meta = data.allWpMeta.edges[0].node.metaFields

  const language = useContext(LanguageContext);

  return (
    <Helmet htmlAttributes={{lang:`${language}`}}>
      <link rel="icon" type="image/x-icon" href={Icon}/>
      <link rel="canonical" href={language === 'zh' ? 'https://chalice.com.tw/' : 'https://chalice.com.tw/en'}/>
      <title>{language === 'zh' ? `${meta.titleCh}` :  `${meta.titleEn}`}</title>
      <meta name="description" 
            content={language === 'zh' ? `${meta.siteDescriptionCh}` : `${meta.siteDescriptionEn}`}/>  
    </Helmet>
  )
}
