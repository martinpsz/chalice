import React, {useContext} from 'react'
import LanguageContext from '../../contexts/Language'
import styled from 'styled-components'
import line from '../../../static/assets/line.svg'
import {AiOutlineMail} from 'react-icons/ai'
import { useStaticQuery, graphql } from 'gatsby'

const StyledAnchor = styled.a`
        padding: 0.5rem;  
    `

export const ContactUs = ({cName}) => {
    const language = useContext(LanguageContext)

    const data = useStaticQuery(graphql`
        {
        allWpMeta {
            edges {
            node {
                metaFields {
                companyLineAppLink
                email
                }
            }
            }
        }
        }
    `)

    
    const Line = data.allWpMeta.edges[0].node.metaFields.companyLineAppLink
    const Email = data.allWpMeta.edges[0].node.metaFields.email

    return (
        <>
            {language === 'zh' ? 
            <StyledAnchor role='button' className={cName} href={Line} target="_blank" rel="noopener noreferrer" aria-controls="Contact Chalice on Line"><img src={line} className='icon' alt="Connect with us on Line" style={{width:'40px'}}/></StyledAnchor> :
            <StyledAnchor role='button' className={cName} href={`mailto:${Email}`} target="_blank" rel="noopener noreferrer" aria-controls="Email Chalice"><AiOutlineMail size={40} color={'var(--yellow)'}/></StyledAnchor>}
        
        </>
    )
         
}