import React, {useContext} from 'react'
import LanguageContext from '../../contexts/Language'
import { graphql, useStaticQuery} from 'gatsby'
import styled from 'styled-components'
import { Trans } from 'react-i18next'
import {Link} from 'react-scroll'



const StyledMenuItems = styled.ul`
    display: flex;
    flex-direction: column;
    height: calc(100vh - 6rem);
    justify-content: space-evenly;
    align-items: center;
    background: var(--blue);
    z-index: 1;
    transform: translateX(-100%);
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    transition: 1s ease-in-out;
    
    &.show{
       transform: translateX(0%);
    }

    @media screen and (min-width: 768px){
        display: flex;
        flex-direction: row;
        width: 100%;
        justify-content: space-evenly;
        border: none;
        height: unset;
        background: unset;
        position: unset;
        transform: unset;
    }
    

    a{
        font-family: ${props => props.option === 'zh' ? "var(--copy-font-ch)" : "var(--copy-font-en)"};
        font-size: 1rem;
        text-transform: uppercase;
        text-decoration: none;
        color: var(--yellow);
        padding: 0.75rem;
        position: relative;
        cursor: pointer;
        

        @media screen and (max-width: 767px){
            &::after{
                content: '';
                position: absolute;
                background: var(--yellow);
                width: 50%;
                height: 1px;
                left : 50%;
                transform: translateX(-50%);
                right: 0;
                bottom: 0;
            }
        }
        
    }

`

export const MenuItems = (props) => {

    const language = useContext(LanguageContext)

    const data = useStaticQuery(graphql`
        query{
            site {
                siteMetadata {
                    menuLinks {
                        name
                        link
                    }
                }
            }
        }
    `)
    const menuLinks = data.site.siteMetadata.menuLinks

    

    const navigation = menuLinks.map((link) =>
        <Link to={link.link}
              smooth={true}
              duration={1000}
              key={link.name} 
              offset={-80}
              onClick={props.menuControl}>
              <Trans>{link.name}</Trans>
        </Link>
    )

    return(
        <>
            <StyledMenuItems className={props.active} option={language}>{navigation}</StyledMenuItems>
        </>
    )

}
