import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import Logo from '../../../static/images/ChaliceGold.webp';
import {AiOutlineAlignLeft} from 'react-icons/ai'
import {ContactUs} from '../../components/Navigation/ContactUs'
import {MenuItems} from '../../components/Navigation/MenuItems';
import {useWindowSize}  from '../useWindowSize';


const StyledNav = styled.nav`
    display: flex;
    justify-content: space-around;
    align-items: center;
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    height: 6rem;
    z-index: 1;

    &.scrolled{
        background-color: var(--blue);
    }

    button{
        background: transparent;
        padding: 0.5rem;
        border: none;
    }

    @media screen and (min-width:768px){
        left: 0;
        right: 0;
        top: 0;
        bottom: unset;
        justify-content: space-between;
        height: unset;
        padding: 0.5rem 2.5rem;
        max-width: 1920px;
        margin: 0 auto;
        

        .hide-on-desktop{
            display: none;
        }
    }

    h1{
        color: var(--yellow);
        font: var(--header-font-en)
    }

    .logo{
        height: 80px;
        width: 80px;

    }

    @media screen and (min-width: 768px){
        .logo{
            height: 100px;
            width: 100px;
        }
    }
`

export const Nav = () => {
    const {width} = useWindowSize()
    const [scroll, setScroll] = useState();
    const [isActive, setActive] = useState(false)
    
    const clickHandler = () => {
        setActive(!isActive)
    } 

    const scrollHandler = () => {
        setScroll(window.scrollY>1)
    }
    
    useEffect(()=>{
        window.addEventListener("scroll", scrollHandler);
        return () => {
            window.removeEventListener("scroll", scrollHandler)
        }
    }, [])
   
    

    return (
    <>
        {width < '768' && <MenuItems active = {isActive === true ? "show" : ""} menuControl={clickHandler}/>} 
        <StyledNav className={scroll && 'scrolled'} >
            <button onClick={clickHandler} className="hide-on-desktop" >
                <AiOutlineAlignLeft size={40} color={'var(--yellow)'}/>
            </button>
            <img className="logo" src={Logo} alt="Chalice Logo"/>
            <ContactUs cName="hide-on-desktop"/>
            {width >= '768' && <MenuItems/>} 
        </StyledNav>
    </>
    )
};
