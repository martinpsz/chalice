import React, {useContext} from 'react'
import styled from 'styled-components'
import LanguageContext from '../contexts/Language'


const StyledSectionHeader = styled.div`
    background: var(--blue);
    display: inline-block;
    color: var(--yellow);
    padding: 0.75rem 1rem;
    margin: 2rem 0 1rem;

    @media screen and (min-width: 768px){
        padding: 1.25rem 1.75rem;
        
    }

    h2{
      font-family: ${props => props.option === 'zh' ? "var(--header-font-ch)" : "var(--header-font-en)"};
      font-size: 2.5rem;
      line-height: 1.3;
      font-weight: 400;
   }

`

export const SectionHeader = (props) => {
  const language = useContext(LanguageContext);
  return (
    <StyledSectionHeader option={language}><h2>{props.children}</h2></StyledSectionHeader>
  )
}

