import React, {useState, useRef, useEffect} from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'
import styled from 'styled-components'
import {SectionHeader} from '../SectionHeader'
import {Trans} from 'gatsby-plugin-react-i18next';
import {GeneralButton} from '../Button'


const StyledPartners = styled.section`
    @media (min-width: 768px){
      margin: 1em 0;
      
    }

    .logo-container{
        display: grid;
        grid-auto-flow: column;
        grid-auto-columns: 66.7%;
        grid-column-gap: 0.5em;
        overflow-x: auto;
        overscroll-behavior-inline: contain;
        scroll-snap-type: inline mandatory;
        scroll-padding-inline: 1rem;
        padding-bottom: 1em;

        @media (min-width: 540px){
          grid-auto-columns: 40%;
        }

        @media (min-width: 768px){
          grid-auto-flow: unset;
          grid-template-columns: repeat(3, 1fr);
          grid-template-rows: auto;
          grid-gap: 0.5em;
        } 

        .image-wrapper{
          scroll-snap-align: start;
          display: flex;
          justify-content: center;
          align-items: center;
          height: 240px;
          
          @media (min-width: 768px){
            scroll-snap-align: unset;
            

            &:nth-of-type(n+10){
              display: none;
            }
            
          }
        }
    }

    button{
      display: none;
      
      @media (min-width: 768px){
        display: block;
        margin-left: 2em;
        margin-bottom: 2em;
      }
    }

    @media (min-width: 1320px){
        max-width: 75vw;
        margin: 0 auto;

        button{
          margin-left: 0;
        }
    }
`

export default function Partners() {

    const data = useStaticQuery(graphql`
    {
      allWpPartner {
        nodes {
          id
          producer_logo_img {
            producerLogo {
              localFile {
                name
                childImageSharp {
                  gatsbyImageData(formats: WEBP, layout: CONSTRAINED,  width: 180)
                }
              }
            }
          }
        }
      }
    }
  `)

  const [reference, setReference] = useState()
  const ref = useRef(null)

  useEffect(()=> {
    setReference(ref.current.querySelectorAll('.image-wrapper:nth-of-type(n+10'))
  }, [])

  const onClickHandler = () => {
    reference.forEach((val) => {
      return val.style.display === 'flex' ? val.style.display = '' : val.style.display = 'flex' 
    })
  }

  

  const Logos = data.allWpPartner.nodes.map(node => {
      return(
          <div key={node.id} className="image-wrapper">
                  <GatsbyImage image={getImage(node.producer_logo_img.producerLogo.localFile.childImageSharp)}
                                          alt={node.producer_logo_img.producerLogo.localFile.name}/>
          </div>
      )
  })

  return (
    <StyledPartners id="Partners">
        <SectionHeader><Trans>Our Partners</Trans></SectionHeader>
        <div className='logo-container' ref={ref}>
            {Logos}
        </div>
        <GeneralButton background='var(--blue)' clickHandler={() => onClickHandler()}>
          <Trans>Toggle Partners</Trans>
        </GeneralButton>
    </StyledPartners>
  )
}
