/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

 require("dotenv").config({
   path: `.env.${process.env.NODE_ENV}`,
 })

module.exports = {
  /**
   * Site Metadata:
   */
  siteMetadata: {
    email: 'chalice.taiwan@gmail.com',
    siteUrl: 'https://chalice.com.tw/',
    lineHandle: '@078jjxuc',
    facebook: 'https://www.facebook.com/chalicetw/',
    phone: '+886-975257733',
    menuLinks: [
      {
        name: 'Our Story',
        link: `About`
      },
      {
        name: 'Our Selection',
        link: 'Selection'
      },
      {
        name: 'Our Partners',
        link: 'Partners'
      },
      {
        name: 'Contact Us',
        link: 'Contact'
      }
    ],
  },
  /* Your site config here */
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-netlify`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
          defaults: {
            formats: ["auto", "webp", "avif"]
          }
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/locales`,
        name: `locale`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/static/images`,
        name: `images`
      }
    },

    {
      resolve: `gatsby-plugin-react-i18next`,
      options: {
        localeJsonSourceName: `locale`,
        languages: ['zh', 'en'],
        defaultLanguage: `zh`,
        siteUrl: process.env.SITE_URL,
        i18nextOptions: {
        keySeparator: false,
        nsSeparator: false
        }
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `birthstone bounce`,
          `montserrat\:100, 200, 300, 400, 500, 800`,
          `noto sans TC\:300, 400`,
          `noto serif TC\:100, 300`,
        ],
        display: 'swap'
      }
    },
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        url: `http://3.36.59.230/graphql`,
      }
    },
    {
      resolve: `gatsby-plugin-robots-txt`,
      options: {
        resolveEnv: () => process.env.GATSBY_ENV,
        env: {
          development: {
            policy: [{userAgent: '*', disallow: ['/']}]
          },
          production: {
            policy: [{userAgent: '*', allow: '/'}]
          }
        }
      }
    },
  ]
}
